var fs = require('fs');
var Log = require('log');
var log = new Log('debug', fs.createWriteStream('search_engine.log'));

log.debug('Program Started');
log.debug('Reading input file');

fs.readFile('nested page input.txt', function (err, data) {
    if(err) {
        log.error('Error while reading input file');
        log.debug('Exiting the program');

        return;
    }

    log.debug('File reading completed');
    takeInput(data.toString().replace(/[\r]+/g, ''));
});

var pages = new Array();
var MAX_ALLOWED_KEYWORD = 8;
var totalQueryCount = 0;

function takeInput(input) {
    log.debug('takeInput started');

    var input1 = input.split('\n');

    for(var i = 0; i < input1.length; i++) {
        var input2 = input1[i].split(' ');

        if(input2[0] == 'P')
            addNewPage(input2);

        else if(input2[0] == 'PP')
            addNewNestedPage(input2);

        else if(input2[0] == 'Q') {
            var query = getQuery(input2);
            getSearchResults(query);
        }
    }

    log.debug('takeInput ended');
}

function addNewPage(arr) {
    log.debug('Adding new page in the pages array');

    var page = {
        id : 'P' + (pages.length + 1),
        keywords : {},
        nestedPages : new Array()
    }

    for(var i = 1; i < arr.length; i++)
        page.keywords[arr[i].toLowerCase()] = MAX_ALLOWED_KEYWORD - i + 1;

    pages.push(page);

    log.debug('page added to the pages array');
    log.debug('pages Array length :', pages.length);
}

function addNewNestedPage(arr) {
    log.debug('Adding new nested page in the pages array');
    var lastPage = pages[pages.length - 1];

    var subPage = {
        id : 'P' + lastPage.id + '.' + (lastPage.nestedPages.length + 1),
        keywords : {}
    }

    for(var i = 1; i < arr.length; i++)
        subPage.keywords[arr[i].toLowerCase()] = MAX_ALLOWED_KEYWORD - i + 1;

    lastPage.nestedPages.push(subPage);

    log.debug('nested page added to the pages array');
    log.debug('subpages Array length :', pages.length);
}

function getQuery(arr) {
    log.debug('Fetching the query object');

    var query = {
        id : 'Q' + (totalQueryCount + 1),
        keywords : new Array()
    }

    for(var i = 1; i < arr.length; i++) {
        var keyword = {
            keyword : arr[i].toLowerCase(),
            score : MAX_ALLOWED_KEYWORD - i + 1
        }

        query.keywords.push(keyword);
    }

    totalQueryCount++;

    log.debug('Getting query completed');
    log.debug('totalQueryCount :', totalQueryCount);

    return query;
}

function getSearchResults(query) {
    log.debug('Getting the search results');

    log.debug(pages);

    var keywords = query.keywords;
    var searchedPage = new Array();

    for(var i = 0; i < pages.length; i++)
        searchedPage.push({ id : pages[i].id, score : 0 });

    log.debug('processing query', query.id);

    for(var i = 0; i < keywords.length; i++) {
        for(var j = 0; j < pages.length; j++) {
            searchedPage[j].score += calculatePageScore(pages[j].keywords, keywords[i]);

            var nestedPages = pages[j].nestedPages;

            var nestedPageScore = 0;

            for(var k = 0; k < nestedPages.length; k++)
                nestedPageScore += calculatePageScore(nestedPages[k].keywords, keywords[i]);

            searchedPage[j].score += (nestedPageScore * (0.1));
        }
    }



    printQueryResult(query, searchedPage);
}

function calculatePageScore(keywords, keyword) {

    log.debug('going through the pages');
    log.debug(keywords, keyword);

    return keywords[keyword.keyword] ? keywords[keyword.keyword] * keyword.score : 0;
}

function printQueryResult(query, searchedPage) {
    log.debug('Printing the query result for query %s', query.id);

    searchedPage.sort(searchedPageComparator);

    log.debug('searchedPage after sorting');
    log.debug('\r', searchedPage);

    process.stdout.write(query.id + ': ');

    var min = Math.min(searchedPage.length, 5);

    for(var i = 0; i < min; i++) {
        if(searchedPage[i].score == 0)
            break;

        process.stdout.write(searchedPage[i].id + ' ');
    }

    process.stdout.write('\n');

    log.debug('Query %s completed', query.id);
}

function searchedPageComparator(a, b) {
    if(a.score != b.score)
        return (b.score - a.score);

    return (a.id < b.id) ? -1 : 1;
}
