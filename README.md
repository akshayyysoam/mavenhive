# MavenHive
Interview Question

Problem Statement is available in the repository as Question.pdf.

# How to start the code

1. You should have node.js installed
2. Download the code
3. Open terminal or command prompt in the folder
4. Run command "node index.js"
5. Output would be seen on the terminal
6. Logs for the code are available in "search_engine.log" file
